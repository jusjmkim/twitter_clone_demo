class SessionsController < ApplicationController
  # GET /login
  def login
  end

  # POST /login
  def login_user
    user = User.find_by(username: params[:username])
    if user && user.password == params[:password]
      session[:user_id] = user.id
      redirect_to user
    else
      redirect_to '/login'
    end
  end

  def logout
    reset_session
    redirect_to users_path
  end
end

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # helper_method allows me to use these methods in views files
  helper_method def logged_in?
    !session[:user_id].nil?
  end

  helper_method def current_user
    @current_user ||= User.find(session[:user_id]) if logged_in?
  end
end
